import React from 'react';
import Cell from './Cell.js'

class Frame extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      cells: Array(9).fill(null),
      turn: 'X',
      count: 0,
      winner: false,
      draw: false,
      timer: 0,
      playerTimer: 0
    };
    this.launchTimer()
  }

  resetPlayerTimer = () => {
    this.setState({
      playerTimer: 0
    })
  }

  isTimeOut = () => {
    if (this.state.playerTimer === 5) {
      this.checkRandomCell();
      this.changePlayer();
      this.resetPlayerTimer();
    }
  }

  changePlayer = () => {
    this.setState({
      turn: (this.state.turn === 'X') ? 'O' : 'X',
      playerTimer: 0
    })
  }

  isOver = () => {
    this.checkWinner();
    this.checkDraw();
  }

  checkWinner = () => {
    let winningCells = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
    winningCells.forEach(sequence => {
      if ((this.state.cells[sequence[0]] === this.state.cells[sequence[1]]) &&
        (this.state.cells[sequence[0]] === this.state.cells[sequence[2]]) &&
        (this.state.cells[sequence[0]] !== null)) {
        this.setState({ winner: this.state.turn })
      }
    });
  }

  checkDraw = () => {
    if ((this.state.count === 8) && this.state.winner === false) {
      this.setState({ draw: true });
    }
  }

  checkRandomCell = () => {
    let emptyCells = []
    this.state.cells.map((cell, index) => {if (!cell) emptyCells.push(index)});    
    let newCells = this.state.cells;

    let indexOfEmptyCell = Math.floor(Math.random() * emptyCells.length);
    
    if (newCells[emptyCells[indexOfEmptyCell]] === null && this.state.winner === false) {
      newCells[emptyCells[indexOfEmptyCell]] = this.state.turn
    } else if ((newCells[emptyCells[indexOfEmptyCell]] === null && this.state.winner)|| (newCells[emptyCells[indexOfEmptyCell]] && this.state.winner)) {
      return ;
    } else {
      this.checkRandomCell();
    }
    this.setState({
      cells: newCells,
      count: this.state.count + 1
    })    

    this.isOver();
  }

  clickOnCell = (cellIndex) => {
    if (this.state.cells[cellIndex] || this.state.winner) {
      return;
    }
    let newCells = this.state.cells;
    newCells[cellIndex] = this.state.turn;

    this.setState({
      cells: newCells,
      count: this.state.count + 1
    })
    this.isOver();
    this.changePlayer();
  }

  displayCells = () => {
    return this.state.cells.map((cell, i) =>
      <Cell key={i} cellValue={cell} clickOnCell={() => this.clickOnCell(i)} />
    )
  }

  launchTimer = () => {
    return setInterval(() => {
      this.setState({
        timer: this.state.timer + 1,
        playerTimer: this.state.playerTimer + 1
      });
      this.isTimeOut();
    }, 1000);
  }

  render() {
    return (
      <div>
        <h2>Timer: {this.state.timer}</h2>
        <p>Player {this.state.turn} is now playing</p>
        <p>Turn: {this.state.count}</p>
        <div>
          {this.state.winner &&
            <h3>{this.state.winner} has won!</h3>}
          {this.state.draw &&
            <p>Draw !</p>}
        </div>
        <div id="frame">
          {this.displayCells()}
        </div>
      </div>
    );
  }
}

export default Frame;
