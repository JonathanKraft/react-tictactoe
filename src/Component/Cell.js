import React from 'react';

class Cell extends React.Component {

  render() {
    return (
      <div className="cell" onClick={this.props.clickOnCell}> {this.props.cellValue} </div>
    );
  }


}

export default Cell;
